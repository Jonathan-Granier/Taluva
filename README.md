# Taluva 

Fait dans le cadre de la Licence d'Informatique à l'Université Grenoble-Alpes. 


---------------------------
### Descriptions
Projet étudiant d'adaptation du jeu de société "Taluva" en jeu vidéo. Durée : 1 mois

* Mise en place d’un Moteur de rendu 3D, d’un moteur gameplay et d’une interface sur plusieurs thread.
* Création de plusieurs Assets et Models 3D.
* Implémentation sous Java et OpenGL.
* Interface faite avec Swing.



![exemple](Images/Taluve_example.png)


---------------------------
### Auteurs

* BIZARD Astor  
* BOUVIER-DENOIX Gabriel  
* GRANIER Jonathan  
* LAWSON Thibault  
* MARONNIER Dimitri  
* WONG Noha  

---------------------------  
### Exécuter le projet :
Aller dans le dossier Exécutable
* Windows : run Taluva.exe
* Linux : run Taluva_unix.jar
